# Technical Report:  Build Tools with Gradle

###Class Assignment 2 (CA2) - Part 1


This technical report was developed in context of **DEVOPS** discipline belonging to the **2019-2020 SWitCH Program** from **I.S.E.P.**.


#### Part I - Project creation: sources and repository

The download of the basic project for this assignment was done from [luisnogueira-gradle_basic_demo-565a2870c9d9](https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/).
The source code and files needed for the assignment has been saved on folder [CA2_Part1](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/).
The following commands presuppose its execution on the console **Windows PowerShell**.

#### Part II - Import the project to IntelliJ® IDEA

##### Step 1: Built the project

First of all we need to build the project, so we must execute the following command line:
```console
$ ./gradlew build
```

##### Step 2: Run the server
Our application (chat) needs a server, so we need to execute:
```console
$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

##### Step 3: Run a client
As a server we must need chat client's, so we must execute:
```console
$ ./gradlew runClient
```

The above task assumes the chat server's IP is "localhost" and its port number is "59001".
That's allows to create just a locally chat (on local computer only).
If you wish to communicate with another server the steps are: change the "localhost" in the task "runClient" of the "build.gradle" file to the IP of the server that you want to chat with you.

#### Part III - Add a new task to execute the server.

The step 2 ran the server through a complex command line.
To simplify the application we can create a gradle script that run the server across as simple task.

##### Step 1: On Intellij Idea open the file "build.gradle"

##### Step 2: Create the task "runServer"
Based on the runClient task, we can made a new task that equally run the server:

[File: buil.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/build.gradle)
```
task runServer(type: JavaExec, dependsOn: classes) {
      classpath = sourceSets.main.runtimeClasspath
      main = 'basic_demo.ChatServerApp'  
      args '59001'
  }
```

#### Part IV - Test and update the gradle script

##### Step 1: Test the new task "runServer"
The script had been created and so we must test the new task:

[Class: AppTest](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/src/test/java/basic_demo/AppTest.java)
```
public class AppTest {

    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }

}
```
##### Step 2: Update dependencies of gradle script
To the previous test and gradlew build have success, we must add the following dependency to build.gradle:

[File: buil.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/build.gradle)
```
dependencies {
(...)

    //Junit for testing
    testCompile 'junit:junit:4.12'
}
```
#### Part V - Add a new task to Copy SRC
 
##### Step 1: Create the task "backupCopy" to make a backup of the sources of the application.

To carry out this task we must add on the buil.gradle file the following script:

[File: buil.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/build.gradle)
```
task backupCopy(type: Copy) {
    from 'src'
    into 'backup'
}
```
##### Step 2: After added the task should be ran.

#### Part VI - Add a new task to Copy SRC as a ZipFile
 
##### Step 1: Create the task "backupZip" to make a backup of the sources of the application as a ZipFile.

To carry out this task we must add on the buil.gradle file the following script:

[File: buil.gradle](https://bitbucket.org/ElisabetedoVale/devops-19-20-a-1191760/src/master/CA2_Part1/gradle_basic_demo/build.gradle)
```
task backupZip(type: Zip) {
       archiveFileName = "project.zip"
       destinationDirectory = file("backup")
   
       from "src"
   }
```
##### Step 2: After added the task should be ran.

####Part VII - Tag the end of assignment 
##### Step 1: Create a new Tag on master "CA2-Part1"

The CA2 - Part1 assignment is completed and so a tag should be created to mark the end of it.
To create the Tag locally, we execute:

```console
$ git tag -a CA2-Part1 -m "CA2-Part1 exercise it's complete"
```
##### Step 2: Add the new Tag to origin
The new tag have been locally created and must sent to the remote repository using the command:

```console
$ git push origin CA2-Part1
```

 This tag mark the end of the assignment.